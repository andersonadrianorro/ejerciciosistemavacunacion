package Modelo;


import java.time.LocalDate;
import java.time.Period;
/**
 * Manejo de fechas:
 * https://www.dev-util.com/java/como-calcular-la-edad-a-partir-de-la-fecha-de-nacimiento-en-java
 * @author madarme
 */
public class Persona {

    private long cedula;

    private String nombre;

    private LocalDate fechaNacimiento;

        
    private String email;

    public Persona() {
    }

    public Persona(long cedula, String nombre, LocalDate fechaNacimiento, String email) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
    }
    
    
    public Persona(long cedula, String nombre, String fechaNacimiento, String email) {
        this.cedula = cedula;
        this.nombre = nombre;
        String datos[]=fechaNacimiento.split("-");
        //1958-12-3
        this.fechaNacimiento=LocalDate.of(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), Integer.parseInt(datos[2]));
        this.email = email;
    }
    

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", fechaNacimiento=" + fechaNacimiento + ", email=" + email + '}';
    }

    
    public int getPrioridad()
    {
    LocalDate actual=LocalDate.now();
    Period periodo=Period.between(fechaNacimiento, actual);
    /*
    System.out.printf("Tu edad es: %s años, %s meses y %s días\n",
                    periodo.getYears(), periodo.getMonths(), periodo.getDays());
    */
    return periodo.getYears()*10000+periodo.getMonths()*100+periodo.getDays();
    
    }
    
    
    
    
    
    
}
